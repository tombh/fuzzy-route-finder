use std::thread;
use route_heatmap;

fn main() {
    let child = thread::Builder::new().stack_size(32 * 1024 * 1024).spawn(move || {
        route_heatmap::run();
    }).unwrap();
    child.join().unwrap();
}
