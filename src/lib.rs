use std::env;
use std::fs::File;
use std::collections::HashMap;
use serde::{Deserialize};

pub mod route_heatmap {
    const TOTAL_AIRPORTS: usize = 10301;
    const MAX_DIRECTS_PER_AIRPORT: usize = 305;
    const MAX_ROUTE_COMBINATIONS: usize = 5000;
    const MAX_HOPS: usize = 5;

    #[derive(Clone, Deserialize, Debug)]
    #[serde(untagged)]
    enum Price {
        Value(f32),
        Null
    }

    #[derive(Deserialize, Debug)]
    struct AirportJSON {
        origin: String,
        destinations: Vec<String>,
        prices: Vec<Price>
    }

    type IndexToIATA = HashMap<u16, String>;
    type IATAToIndex = HashMap<String, u16>;
    type Prices = HashMap<String, HashMap<String, Price>>;
    type Directs = [[u16; MAX_DIRECTS_PER_AIRPORT]; TOTAL_AIRPORTS];

    type DestinationsList = [u16; TOTAL_AIRPORTS];
    type RouteList = [[u16; MAX_HOPS + 1]; MAX_ROUTE_COMBINATIONS];

    // Per-airport
    struct Graph {
        destinations: DestinationsList,
        routes: RouteList
    }

    impl Graph {
        fn new() ->  Self {
            Self {
                destinations: [0; TOTAL_AIRPORTS],
                routes: [[0; MAX_HOPS + 1]; MAX_ROUTE_COMBINATIONS]
            }
        }
    }

    struct Destination {
        route_count: u32,
        price: Price,
        routes: RouteList
    }

    struct Airport {
        iata: String,
        graphs: HashMap<u16, Graph>,
        destinations: HashMap<String, Destination>
    }

    struct RouteHeatmap {
        directs: Directs,
        index_to_iata: IndexToIATA,
        iata_to_index: IATAToIndex
    }

    impl RouteHeatmap {
        fn new() -> Self {
            Self {
                directs: [[0; MAX_DIRECTS_PER_AIRPORT]; TOTAL_AIRPORTS],
                index_to_iata: HashMap::new(),
                iata_to_index: HashMap::new()
            }
        }

        fn run() {
            let json = load_file();
            let mut heatmap = RouteHeatmap::new();
            let mut airports = heatmap.build_core_data(json);
            heatmap.find_all_possible_routes(&mut airports);
            println!("{}", airports[0].iata);
        }

        fn load_file() -> Vec<AirportJSON> {
            let args: Vec<String> = env::args().collect();
            let path = &args[1];
            let file = File::open(path).expect("file should open read only");
            serde_json::from_reader(file).expect("file should be proper JSON")
        }

        fn build_core_data(&mut self, json: Vec<AirportJSON>) -> Vec<Airport> {
            let mut airports: Vec<Airport> = Vec::new();
            for (mius, json_airport) in json.iter().enumerate() {
                let iata = json_airport.origin.to_string();
                let mut master_index = mius as u16;
                master_index += 1;
                self.index_to_iata.insert(master_index, iata.clone());
                self.iata_to_index.insert(iata.clone(), master_index);
            }

            for (mius, json_airport) in json.iter().enumerate() {
                let mut master_index = mius as u16;
                master_index += 1;
                let iata = json_airport.origin.to_string();
                let mut directs = [0; MAX_DIRECTS_PER_AIRPORT];
                let mut destinations = [0; TOTAL_AIRPORTS];
                let mut routes = [[0; MAX_HOPS + 1]; MAX_ROUTE_COMBINATIONS];
                let mut airport = Airport{
                    iata: iata.clone(),
                    destinations: HashMap::new(),
                    graphs: HashMap::new()
                };
                for (i, json_destination) in json_airport.destinations.iter().enumerate() {
                    let destination = json_destination.clone();
                    if !self.iata_to_index.contains_key(&destination) {
                        continue;
                    }
                    let iata_as_index = self.iata_to_index[&destination];
                    let price = json_airport.prices[i].clone();
                    let details = Destination {
                        route_count: 0,
                        routes: [[0; MAX_HOPS + 1]; MAX_ROUTE_COMBINATIONS],
                        price: price
                    };
                    directs[i] = iata_as_index;
                    destinations[i] = iata_as_index;
                    routes[i] = [iata_as_index, 0, 0, 0, 0, 0];
                    airport.destinations.insert(destination, details);
                }
                let graph = Graph {
                    destinations: destinations,
                    routes: routes
                };
                airport.graphs.insert(0, graph);
                self.directs[master_index as usize] = directs;
                airports.push(airport);
            }
            airports
        }

        fn find_all_possible_routes(&self, airports: &mut Vec<Airport>) {
            let max_stops = 2;
            for depth in 0..=max_stops {
                self.all_routes_for_depth(airports, depth);
            }
        }

        fn get_directs_for(&self, index: u16) -> [u16; MAX_DIRECTS_PER_AIRPORT] {
            self.directs[index as usize]
        }

        fn all_routes_for_depth(&self, airports: &mut Vec<Airport>, depth: u16) {
            for airport in airports.iter_mut() {
                self.extend_all_routes_at_depth(airport, depth);
                let eg = airport.graphs[&depth].routes[0];
                println!("{}: {} eg {:?}", depth, airport.iata, eg);
            }
        }

        fn extend_all_routes_at_depth(&self, airport: &mut Airport, depth: u16) {
            let mut graph = Graph::new();
            let current_depth = &airport.graphs[&depth];
            for route in current_depth.routes.iter() {
                let current_end = route.last().unwrap();
                let destinations = self.get_directs_for(*current_end);
                let mut route_index_offset = route.len();
                let mut routes_index_offset = graph.routes.len();
                for new_end in destinations.iter() {
                    if !route.contains(new_end) {
                        route_index_offset += 1;
                        routes_index_offset += 1;
                        let mut extended_route = route.clone();
                        extended_route[route_index_offset as usize] = new_end.to_owned();
                        graph.routes[routes_index_offset as usize] = extended_route;
                    }
                }
            }
            airport.graphs.insert(depth + 1, graph);
        }
    }
}
